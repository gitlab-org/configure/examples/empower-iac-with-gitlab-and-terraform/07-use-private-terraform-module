module "my_mod_1" {
  source  = "gitlab.com/timofurrer/my-private-module/local"
  version = "~> 4.0"
}

module "my_mod_2" {
  source  = "gitlab.com/timofurrer/my-private-module-2/local"
  version = "~> 6.0"
}

output "pet_name_1" {
  value = module.my_mod_1.pet_name
}

output "pet_name_2" {
  value = module.my_mod_2.pet_name
}